#!/usr/bin/env node

const articles = require('./articles.json')
const categories = require('./categories.json')
const faker = require('faker')

const articleCategories = []
for (let i = 0; i < articles.length; i++) {
    let ac = {
        id: faker.random.uuid(),
        articlId: articles[i].id,
        categoryId: categories[i].id,
        createdAt: faker.date.recent().getTime(),
        updatedAt: faker.date.recent().getTime(),
    }

    articleCategories.push(ac)
}

console.log(JSON.stringify(articleCategories, undefined, 2))