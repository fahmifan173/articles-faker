#!/usr/bin/env node
const faker = require('faker')
// const exec = require('child_process').execSync

let news_arr = []

for (let i = 0; i < 1000; i++) {
    let news = {
        id: faker.random.uuid(),
        userId: "00000000-0000-0000-0000-000000000001",
        title: "Lorem ipsum dolor sit amet",
        slug: "",
        body: faker.lorem.words(50),
        bodyHtml: "{\"object\":\"value\",\"document\":{\"object\":\"document\",\"data\":{},\"nodes\":[{\"object\":\"block\",\"type\":\"bold\",\"data\":{},\"nodes\":[{\"object\":\"text\",\"text\":\"Lipsum 2\",\"marks\":[]}]},{\"object\":\"block\",\"type\":\"line\",\"data\":{},\"nodes\":[{\"object\":\"text\",\"text\":\"\",\"marks\":[]}]},{\"object\":\"block\",\"type\":\"line\",\"data\":{},\"nodes\":[{\"object\":\"text\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\",\"marks\":[]}]},{\"object\":\"block\",\"type\":\"line\",\"data\":{},\"nodes\":[{\"object\":\"text\",\"text\":\"\",\"marks\":[]}]},{\"object\":\"block\",\"type\":\"line\",\"data\":{},\"nodes\":[{\"object\":\"text\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\",\"marks\":[]}]}]}}",
        status: "published",
        imageUrl: faker.image.imageUrl(),
        createdAt: faker.date.recent().getTime(),
        updatedAt: faker.date.recent().getTime(),
    }
    news_arr.push(news)
}

console.log(JSON.stringify(news_arr, undefined, 2))

