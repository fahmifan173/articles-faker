#!/usr/bin/env node
// const articles = require('./articles.json')
const faker = require('faker')

const categories = []
for (let i = 0; i < 1000; i++) {
    let category = {
        id: faker.random.uuid(),
        userId: "00000000-0000-0000-0000-000000000001",
        name: faker.random.word(),
        slug: faker.random.uuid(),
        default: 0,
        createdAt: faker.date.recent().getTime(),
        updatedAt: faker.date.recent().getTime(),
    }

    categories.push(category)
}

console.log(JSON.stringify(categories, undefined, 2))